$(document).ready(function ()
{

  function PlaySound(soundobj) {
    var thissound=document.getElementById(soundobj);
    thissound.play();
  }

  function PauseSound(soundobj) {
    var thissound=document.getElementById(soundobj);
    thissound.pause();
  }

  function PlayVideo(videoobj) {
    var thisvid=document.getElementById(videoobj);
    thisvid.play();
  }

  function PauseVideo(videoobj) {
    var thisvid=document.getElementById(videoobj);
    thisvid.pause();
  }

  var volumeRange = $("#volume");
  volumeRange.change(function () {
    $("audio").prop("volume", (volumeRange.val()/100));
    $("audio.disabled").prop("volume", "0");
    $("path").attr("status","enabled");
    $("path").removeClass("disabled")
  });

  $(".application-inside").click(function(){
    var audio_id = "#audio" + $(this).attr("id");
    if ($(this).attr('status') == "enabled"){
      $(audio_id).prop("volume", "0");
      $(this).attr("status","disabled");
      $(this).addClass("disabled");
    } else {
      $(audio_id).prop("volume", (volumeRange.val()/100));
      $(this).attr("status","enabled");
      $(this).removeClass("disabled");
    }
  });


  $('.play, .pause').click(function(){
    $(".play, .pause").removeClass("active");
    $(this).addClass("active");
  });

  $(".reset").click(function(){
    $("audio").prop("volume", (volumeRange.val()/100));
    $("path").attr("status","enabled");
    $("path").removeClass("disabled")
  });

  // Stop all.
  $(".pause").click(function(){
    PauseSound('audio0');
    PauseSound('audio1');
    PauseSound('audio2');
    PauseSound('audio3');
    PauseSound('audio4');
    PauseSound('audio5');
    PauseSound('audio6');
    PauseSound('audio7');
    PauseSound('audio8');
    PauseSound('audio9');
    PauseSound('audio10');
    PauseSound('audio11');
    PauseSound('audio12');
    PauseSound('audio13');
    PauseSound('audio14');
    PauseVideo('video');
    PauseVideo('video2');
  });

  // Play all.
  $(".play").click(function(){
    PlaySound('audio0');
    PlaySound('audio1');
    PlaySound('audio2');
    PlaySound('audio3');
    PlaySound('audio4');
    PlaySound('audio5');
    PlaySound('audio6');
    PlaySound('audio7');
    PlaySound('audio8');
    PlaySound('audio9');
    PlaySound('audio10');
    PlaySound('audio11');
    PlaySound('audio12');
    PlaySound('audio13');
    PlaySound('audio14');
    PlayVideo('video');
    PlayVideo('video2');
  });
});

$(window).ready(function(){
  $('.loading').toggle();
  $('.application-container').toggle();
});